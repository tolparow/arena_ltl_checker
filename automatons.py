from itertools import product


class ABSAutomaton:
    """
    Abstract automaton. Could possibly help in future to add new automatons.
    """

    def __init__(self, init_states: set):
        """
        Create empty automaton with some initial states.

        :param init_states: initial states of the automaton
        """
        self.states = init_states.copy()
        self.init_states = init_states.copy()
        self.transitions = dict()


class KripkeStructure(ABSAutomaton):
    """
    Transition System in for of Kripke Structure.
    """

    def __init__(self, init_states: set, alphabet: set):
        """
        Create a new Kripke Structure.

        :param init_states: initial states of the TS
        :param alphabet: alphabet (AP) of the TS
        """
        super().__init__(init_states)
        self.labeling = dict()
        self.alphabet = alphabet.copy()

    def add_state_atomic_proposition(self, state: str, atomic_proposition: str or None):
        """
        Add atomic proposition to some state.

        :param state: state to modify
        :param atomic_proposition: atomic proposition to add
        """
        if atomic_proposition is not None:
            self.alphabet.add(atomic_proposition)

        self.states.add(state)

        state_props = self.labeling.get(state)
        if state_props is None:
            self.labeling[state] = dict()

        if atomic_proposition is not None:
            self.labeling[state][atomic_proposition] = True

    def add_transition(self, state_from: str, state_to: str):
        """
        Add unconditional transition.

        :param state_from: start state
        :param state_to: end state
        """
        self.add_state_atomic_proposition(state_to, None)
        self.add_state_atomic_proposition(state_from, None)

        from_dict = self.transitions.get(state_from)
        if from_dict is None:
            self.transitions[state_from] = set()

        self.transitions[state_from].add(state_to)

    def compile(self) -> 'KripkeStructure':
        """
        Add negative atomic propositions for each state to complete it
        in order for the future usage of it in boolean formulae evaluation.

        :return: self
        """
        for state in self.states:
            state_aps = self.labeling[state]

            self.labeling[state].update({s: False for s in self.alphabet - state_aps.keys()})

        return self

    def __str__(self):
        return f'States: {self.states}\nInit: {self.init_states}\n' \
               f'Labels: {self.labeling}\nAlphabet: {self.alphabet}\nTransitions: {self.transitions}'


class NondeterministicBuchiAutomaton(ABSAutomaton):
    def __init__(self, init_states: set, acceptance_states: set):
        """
        Create a new NBA.

        :param init_states: initial states of the NBA
        :param acceptance_states: acceptance states of the NBA
        """
        super().__init__(init_states)
        self.acceptance_states = acceptance_states

    def add_transition(self, state_from: str, state_to: str, inp: str):
        """
        Save a new transition in a self.transitions dictionary in such a way:
        self.transitions[state_from][inp] = set(state_to_1, state_to_2...)

        :param state_from: start state
        :param state_to: end state
        :param inp: input for the transition
        """
        self.states = self.states.union({state_from, state_to})

        from_dict = self.transitions.get(state_from)
        if from_dict is None:
            self.transitions[state_from] = dict()

        input_dict = self.transitions[state_from].get(inp)
        if input_dict is None:
            self.transitions[state_from][inp] = set()

        self.transitions[state_from][inp].add(state_to)

    def get_product_automaton(self, kripke_structure: KripkeStructure):
        """
        The algorithm of product is given from presentation #4 page 11.

        :param kripke_structure: TS
        :return: PA = TS * self (NBA)
        """
        # Find states
        states = set(product(kripke_structure.states, self.states))

        # Find initial states in a not very efficient, but working way
        initial_states = set()
        for s_0 in kripke_structure.init_states:
            for q_0 in self.init_states:
                for q_0_formula in self.transitions[q_0]:
                    if eval(q_0_formula, kripke_structure.labeling[s_0]):
                        initial_states = initial_states.union({(s_0, q) for q in self.transitions[q_0][q_0_formula]})

        # Find acceptance states
        acceptance_states = set(product(kripke_structure.states, self.acceptance_states))

        # Create PA
        product_automaton = NondeterministicBuchiAutomaton(initial_states, acceptance_states)
        product_automaton.states = states

        # Find a transition function (again, not very efficient)
        transition_function = dict()
        for s in kripke_structure.states:
            for q in self.states:
                transition_function[(s, q)] = dict()
                transition_function[(s, q)][True] = set()

                s_nexts = kripke_structure.transitions[s]
                for s_n in s_nexts:
                    for q_formula in self.transitions[q]:
                        if eval(q_formula, kripke_structure.labeling[s_n]):
                            transition_function[(s, q)][True] = transition_function[(s, q)][True].union(
                                {(s_n, q_n) for q_n in self.transitions[q][q_formula]}
                            )
        product_automaton.transitions = transition_function

        return product_automaton

    def has_accepted_paths(self) -> bool:
        """
        Check if NBA has accepted path.
        Used of verification.

        :return: True if has accepted path else False
        """

        def dfs_find_path(start, end):
            """
            Generator that finds a path between two states.

            :param start: start state
            :param end: end state
            :return: path
            """
            fringe = [(start, [])]
            while fringe:
                state, path = fringe.pop()
                if path and state == end:
                    yield path
                    continue
                for next_state in self.transitions[state][True]:
                    if next_state in path:
                        continue
                    fringe.append((next_state, path + [next_state]))

        visited_acceptance_states = {path[-1]
                                     for init_state in self.init_states
                                     for acc_state in self.acceptance_states
                                     for path in dfs_find_path(init_state, acc_state)}

        cycled_acceptance_states = {path[-1]
                                    for acc_state in self.acceptance_states
                                    for path in dfs_find_path(acc_state, acc_state)}

        # Return if there an infinitely going through acceptance state path, starting from the initial one
        return len(visited_acceptance_states.intersection(cycled_acceptance_states)) > 0

    def __rmul__(self, other: KripkeStructure) -> 'NondeterministicBuchiAutomaton':
        """
        Implement cross product operation.
        This allows usage of multiplication between KripkeStructure and NondeterministicBuchiAutomaton.

        :param other: TS
        :return: TS * self
        """
        return self.get_product_automaton(other)

    def __str__(self):
        return f'States: {self.states}\nInit: {self.init_states}\n' \
               f'Accept: {self.acceptance_states}\nTransitions: {self.transitions}'


# Here comes testing
if __name__ == '__main__':
    nba = NondeterministicBuchiAutomaton({'q0'}, {'q1'})

    nba.add_transition('q0', 'q1', 'a')
    nba.add_transition('q0', 'q0', 'True')

    nba.add_transition('q1', 'q1', 'True')

    # print(nba)

    ts = KripkeStructure({'s0'}, {'a', 'b'})
    ts.add_transition('s0', 's1')
    ts.add_transition('s1', 's1')

    ts.add_state_atomic_proposition('s1', 'a')

    ts.compile()

    # print(ts)

    pa = ts * nba

    print(pa)

    print(pa.has_accepted_paths())
