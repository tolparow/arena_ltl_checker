from spin_parser import ltl_to_nba
from automatons import KripkeStructure


class ArenaSystem:
    """
    This class combines arena and all its specifications.
    """

    def __init__(self, input_file_name: str, output_file_name: str):
        """
        Create Arena system, Arena and specification automatons,
        while parsing the input file.

        :param input_file_name: file to read statements from
        :param output_file_name: file to output results to
        """
        self.specification_automatons = []

        self.output_file_name = output_file_name

        with open(input_file_name, 'r') as input_file:
            n, m = (int(s) for s in input_file.readline().split(' '))

            self.arena = Arena(n, m)

            if input_file.readline().strip() != 'Propositions':
                raise ValueError('`Propositions` title is missing')

            # Add propositions
            while True:
                proposition = input_file.readline().strip()

                if proposition == 'Formulas':
                    break

                proposition = proposition.split(' ')
                self.arena.add_proposition(proposition[0], int(proposition[1]), int(proposition[2]))

            # Add LTL formulas
            for ltl in input_file:
                if ltl.strip() != '':
                    self.specification_automatons.append(ltl_to_nba(ltl))

    def check_satisfiable(self):
        """
        Check if all specifications given are satisfiable.
        """
        with open(self.output_file_name, 'w') as output_file:
            for specification in self.specification_automatons:
                self.arena.ts.compile()
                holds = (self.arena.ts * specification).has_accepted_paths()

                output_file.write('1\n' if holds else '0\n')


class Arena:
    """
    This class represents Arena.
    """

    def __init__(self, n: int, m: int):
        """
        Create an empty Arena and corresponding KripkeStructure.

        :param n: length of the arena
        :param m: width of the arena
        """
        self.n = n
        self.m = m

        self.ts = KripkeStructure(set(), {'o', 'c', 'p', 'd', 'i'})

        # Add transitions
        for i_n in range(1, n + 1):
            for i_m in range(1, m + 1):
                self.ts.add_transition(f'{i_n}_{i_m}', f'{i_n}_{i_m}')
                if i_n + 1 <= n:
                    self.ts.add_transition(f'{i_n}_{i_m}', f'{i_n + 1}_{i_m}')
                if i_n - 1 >= 1:
                    self.ts.add_transition(f'{i_n}_{i_m}', f'{i_n - 1}_{i_m}')
                if i_m + 1 <= m:
                    self.ts.add_transition(f'{i_n}_{i_m}', f'{i_n}_{i_m + 1}')
                if i_m - 1 >= 1:
                    self.ts.add_transition(f'{i_n}_{i_m}', f'{i_n}_{i_m - 1}')

    @staticmethod
    def _get_state_name(i_n: int, i_m: int) -> str:
        """
        Return a state name from to integers, by combining them using underscore.

        :param i_n: 1st coordinate
        :param i_m: 2nd coordinate
        :return: 1_2 like string
        """
        return f'{i_n}_{i_m}'

    def add_proposition(self, proposition: str, i_n: int, i_m: int):
        """
        Add an atomic proposition to some state.

        :param proposition: proposition to be added
        :param i_n: 1st coordinate of the state
        :param i_m: 2nd coordinate of the state
        """
        state = Arena._get_state_name(i_n, i_m)

        if proposition == 'i':
            self.ts.init_states.add(state)

        self.ts.add_state_atomic_proposition(state, proposition)


# Here comes testing
if __name__ == '__main__':
    arena_system0 = ArenaSystem('test_inputs/input0.txt', 'test_inputs/input0.txt.out')
    arena_system0.check_satisfiable()
