import re
import subprocess

from automatons import NondeterministicBuchiAutomaton


def convert_promela_expression_to_python(promela_expression: str) -> str:  # TODO check if covers all cases
    """
    Converts PROMELA expression to Python boolean expression.
    Allows future use of built-in eval(formula) to evaluate it without any 3rd-party software.

    :param promela_expression: expression to be converted
    :return: string with converted expression
    """
    return promela_expression \
        .replace('&&', 'and') \
        .replace('||', 'or') \
        .replace('!', 'not ')


def parse_spin_never_statement(never_statement: str) -> NondeterministicBuchiAutomaton:
    """
    Simply parses SPIN never statements to Kripke Structure.

    :param never_statement: string of statement, received from ltl3ba
    :return: parsed Kripke Structure
    """
    states = set()
    init_states = set()
    accept_states = set()
    transitions = dict()

    current_state = None

    lines, i = [l.strip() for l in never_statement.split('\n')][1:-1], 0

    for line in lines:
        if line[:2] == '::' or line in ['skip', 'false;']:
            if line == 'skip':
                expression = 'True'
                next_state = current_state
            elif line == 'false;':
                expression = 'False'
                next_state = current_state
            else:
                expression = re.search('\((.*)\)', line).group(1)  # TODO check embedded parentheses support
                expression = convert_promela_expression_to_python(expression)

                next_state = line.split('goto')[1].strip()

            if current_state not in transitions:
                transitions[current_state] = dict()
            if expression not in transitions[current_state]:
                transitions[current_state][expression] = set()

            transitions[current_state][expression].add(next_state)

        elif line in ['if', 'fi;', '}']:
            pass

        else:
            current_state = line[:-1]

            states.add(current_state)

            if current_state.find('init') >= 0:
                init_states.add(current_state)

            if current_state.find('accept') >= 0:
                accept_states.add(current_state)

    automaton = NondeterministicBuchiAutomaton(init_states, accept_states)
    automaton.states = states
    automaton.transitions = transitions

    return automaton


def ltl_to_nba(ltl_formula: str, negate: bool = False) -> NondeterministicBuchiAutomaton:
    """
    Convert LTL formula to NBA.

    :require: ltl3ba installed and accessible without sudo
    :param ltl_formula: LTL to be parsed
    :param negate: negate formula before parsing?
    :return: NBA
    """
    ltl_formula = ltl_formula if not negate else f'!({ltl_formula})'

    spin_never_statement = subprocess.run(['ltl3ba', '-f', f'"{ltl_formula}"'], stdout=subprocess.PIPE) \
        .stdout.decode("utf-8")

    return parse_spin_never_statement(spin_never_statement)


# Here comes testing
if __name__ == '__main__':
    nvr1 = """never { /* F c && G ( c -> ( X d || X X d ) ) */
    T0_init:
            if
            :: (!c) -> goto T0_init
            :: (c) -> goto accept_S3
            fi;
    accept_S1:
            if
            :: (!c) -> goto accept_S1
            :: (c) -> goto accept_S3
            fi;
    accept_S2:
            if
            :: (!c && d) -> goto accept_S1
            :: (c && d) -> goto accept_S3
            fi;
    accept_S3:
            if
            :: (!c && d) -> goto accept_S1
            :: (!d) -> goto accept_S2
            :: (c && d) -> goto accept_S3
            fi;
    }"""

    automaton1 = parse_spin_never_statement(nvr1)
    print(automaton1, "\n\n\n")

    nvr2 = """never { /* F a */
    T0_init:
        if
        :: (!a) -> goto T0_init
        :: (a) -> goto accept_all
        fi;
    accept_all:
        skip
    }"""

    automaton2 = parse_spin_never_statement(nvr2)
    print(automaton2, '\n\n\n')

    nvr3 = """never { /* G a */
    accept_init:
        if
        :: (a) -> goto accept_init
        fi;
    }
    """

    automaton3 = parse_spin_never_statement(nvr3)
    print(automaton3, '\n\n\n')

    ltl = 'G a'

    automaton4 = ltl_to_nba(ltl)
    print(automaton4)
