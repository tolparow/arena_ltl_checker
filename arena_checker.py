import sys

from system import ArenaSystem

"""
Main program. Run "python3 arena_checker in_file out_file".
"""

# Read input file names
in_file, out_file = sys.argv[1:]

# Check
arena_system0 = ArenaSystem(in_file, out_file)
arena_system0.check_satisfiable()
